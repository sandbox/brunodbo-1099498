; Media Demo Profile makefile
; ---------------------------

; Core version
; ------------
core = 7.x

; Drush Make API version
; ----------------------
api = 2

; Projects to build the media profile.
; See http://drupal.org/project/media. Since this is a Development
; profile, we're using dev versions of all media related modules.
; ----------------------------------------------------------------

; Download the 'Media' project.
projects[media][version] = "2.0-unstable6"

; Required contrib modules
projects[ctools][version] = "1.0"
projects[file_entity][version] = "2.0-unstable6"
projects[views][version] = "3.3"

; Optional contrib modules
projects[wysiwyg][version] = "2.x-dev"
projects[media_flickr][version] = "1.0-alpha1"
projects[media_youtube][version] = "1.0-beta3"
projects[media_vimeo][version] = "1.0-beta4"
projects[media_bliptv][version] = "1.x-dev"
projects[media_archive][version] = "1.0-beta1"
projects[plupload][version] = "1.0"
projects[libraries][version] = "1.0"

; External libraries (go into into /sites/all/libraries)
; ------------------------------------------------------

; CKEditor
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

; Plupload library
libraries[plupload][download][type] = "get"
libraries[plupload][download][url] = "https://github.com/downloads/moxiecode/plupload/plupload_1_5_2.zip"
libraries[plupload][directory_name] = "plupload"
libraries[plupload][destination] = "libraries"
