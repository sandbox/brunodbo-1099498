Media Demo profile
------------------

== What does it do ==

The profile currently gets your D7 install configured to insert images into a text area via ckeditor.

== How to install ==

1. Clone the project into an existing Drupal installation's /profiles directory.

2. From within /profiles/media_demo, run:

drush make media_demo.make --no-core --contrib-destination=.

3. Install the site as usual, using the 'Media demo' profile as the installation profile.
